pipeline {
    agent any

    environment {
		registry = "rpaschoalini/vinhedo"
        registryCredential = "passworddocker"
        dockerImage = ''
    }

    stages {
    	stage('Clone Repository') {
    		steps {  
                git url: 'https://gitlab.com/rpaschoalini/vinhedo.git'
			}
    	}
    	stage('Build Docker Image') {
            steps{
                script {
                    dockerImage = docker.build registry + ":$BUILD_NUMBER"
                }
            }
        }
    	stage('Send image to Docker Hub') {
            steps{
                script {
                    docker.withRegistry( '', registryCredential) {
                        dockerImage.push()
                    }
                }
            }
        }
    	stage('Deploy') {
		    steps{
                step([$class: 'AWSCodeDeployPublisher',
                    applicationName: 'jenkins',
                    awsAccessKey: "AKIAVIR226F4AYA6PN6W",
                    awsSecretKey: "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",
                    credentials: 'awsAccessKey',
                    deploymentGroupAppspec: false,
                    deploymentGroupName: 'jenkins',
                    deploymentMethod: 'deploy',
                    excludes: '',
                    iamRoleArn: '',
                    includes: '**',
                    pollingFreqSec: 15,
                    pollingTimeoutSec: 600,
                    proxyHost: '',
                    proxyPort: 0,
                    region: 'us-east-1',
                    s3bucket: 's3.rpaschoalini', 
                    s3prefix: '', 
                    subdirectory: '',
                    versionFileName: '',
                    waitForCompletion: true])
            }
        }
    	stage('Cleaning up') {
        	steps {
            	sh "docker rmi $registry:develop"
        	}
		}
    }
}